#!/system/bin/sh
testDir="/private/cslc/tscsclient/";
if [ ! -d "$testDir" ];then
/system/bin/busybox tar xvf /system/etc/easypos.tar     -C /private
/system/bin/busybox tar xvf /system/etc/driver.tar      -C /private/cslc/easypos
/system/bin/busybox tar xvf /system/etc/tscsclient.tar  -C /private
echo "[EC0100001]
input_dir=/private/cslc/easypos/driver/EC0100/001/
output_dir=/private/cslc/easypos/driver/EC0100/001/log
printer=/private/cslc/easypos/driver/EC0100/001/libHWISzztPrinter.so
scanner=
hscanner=/private/cslc/easypos/driver/EC0100/001/libHWIILOTBCR.so
ups=
led=
cdu=
hwinfo=/private/cslc/easypos/driver/EC0100/001/libHWInfoQuery.so" >> /private/cslc/easypos/config/driver.ini
echo "[ELSE]
printer=/private/cslc/easypos/driver/EC0100/001/libHWISzztPrinter.so
scanner=
hscanner=/private/cslc/easypos/driver/EC0100/001/libHWIILOTBCR.so
ups=
led=
cdu=
hwinfo=/private/cslc/easypos/driver/EC0100/001/libHWInfoQuery.so" >> /private/cslc/easypos/config/driver.ini
echo "[TEST]
printer=/private/cslc/easypos/driver/EC0100/001/libHWISzztPrinter.so
scanner=
hscanner=/private/cslc/easypos/driver/EC0100/001/libHWIILOTBCR.so
ups=
led=
cdu=
ntp=172.26.13.85  8888
hwinfo=/private/cslc/easypos/driver/EC0100/001/libHWInfoQuery.so" >> /private/cslc/easypos/config/driver.ini
chmod 0777 /private/ -R
chown system:system /private/ -R
/system/bin/am force-stop com.cslc.easypos
fi
