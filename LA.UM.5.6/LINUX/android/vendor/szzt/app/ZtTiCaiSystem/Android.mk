LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := ZtTiCaiSystem
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(LOCAL_MODULE).apk
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := .apk
LOCAL_PREBUILT_JNI_LIBS:= \
     @lib/armeabi/libqcomnvram.so
LOCAL_CERTIFICATE := platform
LOCAL_DEX_PREOPT    := false
LOCAL_PROGUARD_ENABLED := disabled
include $(BUILD_PREBUILT) 

