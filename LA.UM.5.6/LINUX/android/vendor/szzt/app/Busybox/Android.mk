LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)  
LOCAL_MODULE        := busybox  
LOCAL_MODULE_TAGS   := optional  
LOCAL_MODULE_CLASS  := EXECUTABLES 
LOCAL_SRC_FILES     := busybox
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)