LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := ZtTiCaiLocationService
LOCAL_SRC_FILES := $(LOCAL_MODULE).apk
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_PATH := $(PRODUCT_OUT)/system/app/
LOCAL_MODULE_SUFFIX := .apk
LOCAL_PREBUILT_JNI_LIBS:= \
@lib/armeabi/libapp_BaiduNaviApplib.so\
@lib/armeabi/libBaiduMapSDK_map_v3_7_1.so\
@lib/armeabi/libbd_etts.so\
@lib/armeabi/libcurl.so\
@lib/armeabi/libapp_BaiduVIlib.so\
@lib/armeabi/libBaiduMapSDK_search_v3_7_1.so\
@lib/armeabi/libBDSpeechDecoder_V1.so\
@lib/armeabi/libgnustl_shared.so\
@lib/armeabi/libBaiduMapSDK_base_v3_7_1.so\
@lib/armeabi/libBaiduMapSDK_util_v3_7_1.so\
@lib/armeabi/libbds.so\
@lib/armeabi/liblocSDK6a.so
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_DEX_PREOPT    := false
LOCAL_PROGUARD_ENABLED := disabled
include $(BUILD_PREBUILT) 

