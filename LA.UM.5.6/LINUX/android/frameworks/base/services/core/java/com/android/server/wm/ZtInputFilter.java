/******************************************************************************

  Copyright (C), 2001-2011, DCN Co., Ltd.

 ******************************************************************************
  File Name     : ZtInputFilter.java
  Version       : Initial Draft
  Author        : yixing
  Created       : 2017/12/15
  Last Modified :
  Description   : ZT InputFilter
  Function List :
  History       :
  1.Date        : 2017/12/15
    Author      : yixing
    Modification: Created file

******************************************************************************/

/*----------------------------------------------*
 * external variables                           *
 *----------------------------------------------*/

/*----------------------------------------------*
 * external routine prototypes                  *
 *----------------------------------------------*/

/*----------------------------------------------*
 * internal routine prototypes                  *
 *----------------------------------------------*/

/*----------------------------------------------*
 * project-wide global variables                *
 *----------------------------------------------*/

/*----------------------------------------------*
 * module-wide global variables                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * constants                                    *
 *----------------------------------------------*/

/*----------------------------------------------*
 * macros                                       *
 *----------------------------------------------*/

/*----------------------------------------------*
 * routines' implementations                    *
 *----------------------------------------------*/
package com.android.server.wm;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.view.IInputFilter;
import android.view.InputFilter;
import android.view.InputEvent;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.IZtInputFilter;
import android.view.InputDevice;
import android.view.WindowManager;
import android.view.WindowManagerInternal;
import com.android.server.LocalServices;

public class ZtInputFilter extends InputFilter{
	private static ZtInputFilter mZtInputFilter = null;
	private Context mContext=  null;
	private final WindowManagerInternal mWindowManagerService;
	private IZtInputFilter mIZtInputFilter = null;
	private ZtInputFilter(Context context){
		super(context.getMainLooper());   
		mContext = context;
		mWindowManagerService = LocalServices.getService(WindowManagerInternal.class);
		if(mWindowManagerService != null){
			mWindowManagerService.setInputFilter(this);
		}
	}
	
	public synchronized static ZtInputFilter getInstance(Context context){
        if(mZtInputFilter == null){
            mZtInputFilter = new ZtInputFilter(context);
        }
        return mZtInputFilter;
    }

	public void setZtInputFilter(IZtInputFilter ztInputFilter){
		if(ztInputFilter != null){
			mIZtInputFilter = ztInputFilter;
		}
	}
	
	@Override
    public void onInputEvent(InputEvent event, int policyFlags) {
    	if(mIZtInputFilter == null){
			super.onInputEvent(event, policyFlags);
		}else{
			if (event instanceof MotionEvent
	                && event.isFromSource(InputDevice.SOURCE_TOUCHSCREEN)) {
	            MotionEvent motionEvent = (MotionEvent) event;
				float x = motionEvent.getRawX();
				float y = motionEvent.getRawY();

				try {
	                if(!mIZtInputFilter.onInputFilterMotionEvent((MotionEvent)event, policyFlags)){
						super.onInputEvent(event, policyFlags);
					}
	            } catch (RemoteException re) {
					super.onInputEvent(event, policyFlags);
	            }
	        }else if (event instanceof KeyEvent
	                && event.isFromSource(InputDevice.SOURCE_KEYBOARD)) {
	            KeyEvent keyEvent = (KeyEvent) event;

				try {
	                if(!mIZtInputFilter.onInputFilterKeyEvent(keyEvent, policyFlags)){
						super.onInputEvent(event, policyFlags);
					}
	            } catch (RemoteException re) {
					super.onInputEvent(event, policyFlags);
	            }
	        } else {
	            super.onInputEvent(event, policyFlags);
	        }
		}
    	/*
        if (event instanceof MotionEvent
                && event.isFromSource(InputDevice.SOURCE_TOUCHSCREEN)) {
            MotionEvent motionEvent = (MotionEvent) event;
			float x = motionEvent.getRawX();
			float y = motionEvent.getRawY();

			super.onInputEvent(event, policyFlags);
        } else if (event instanceof KeyEvent
                && event.isFromSource(InputDevice.SOURCE_KEYBOARD)) {
            KeyEvent keyEvent = (KeyEvent) event;

			super.onInputEvent(event, policyFlags);
        } else {
            super.onInputEvent(event, policyFlags);
        }
        */
    }
}

