// IZtSystemServer.aidl
package com.szzt.ztticaisystem;
import android.content.Intent;
// Declare any non-default types here with import statements
/** {@hide} */
interface IZtSystemServer {
    boolean isSupportAppManager();
    int appInstall(String apkPath);
    int appUpdate(String apkPath);
    int appUninstall(String packageName);
    boolean isSupportAppDataManager();
    void cleanAppData(String appPkgName);
    int appInstallAndStart(String apkPath, String clsName);
    int startApp(String pkgName, String clsName);

    int setSystemHistorySaveDir(String dirname);
    boolean writeOperationLog(String msg);
    boolean writeUpdateLog(String msg);
    boolean writeAppsLog(String msg);
    boolean writeAuthorityLog(String msg);
    int getLastOperationErrorCode();
    String getLastOperationErrorString();

    int isNetworkAvailable(int type);
    boolean enableNetwork(int type);
    boolean disableNetwork(int type);
    int isNetworkEnable(int type);
    boolean openNetwork(int type);
    boolean closeNetwork(int type);
    int isNetworkOpen(int type);
    int currentNetworkType();
    boolean setNetworkMode(int type , int mode,in Intent parameters);
    int getNetworkMode(int type);
    Intent getNetworkParameters(int type,int mode);

    int msecFromLastOperation(int opertype);
    boolean startAdbServer(int port);
    boolean stopAdbServer();
    boolean setSdCardState(boolean isEnable);
    int getSdCardState();
    int getFlashCapacity(int flag,String path);
    void reboot();
    void poweroff();
    boolean autoShutDown(int year,int month,int day,int hour,int min,int sec,int msec);
    String screenshot(int index, String fileName);
    int clearDevice();
    void factoryReset();
    boolean firmwareDownloadFromFile(String upgradeFilePath);
    String lastUpdateResult();
    String getFirmwareInformation();
    int lock(String passwd);
    int unlock(String passwd);
    int unlockFailedAttempts();

    boolean setNtpServer(String server,int port);
    boolean syncTime();
    boolean restartNtp();
    boolean setTime(int year,int month,int day,int hour,int min,int sec,int msec);
    boolean setTimeZt(String pkgName,int year,int month,int day,int hour,int min,int sec,int msec);

    Intent getEthernetDhcpNetworkParameters();
    void setDisplaySizeForZt(int displayId, int width, int height);
    String checkSystemUpdate2(String cfgFile);
    byte[] readSn();
    byte[] readProductDate();
    int writeNvData(int item,in byte[] data,int len);

    boolean IPSecDial(in Intent intent);
    boolean ADSLDial(in Intent intent);
    boolean disconnectDial(int type);
    boolean adslIsConnect();
    String getPkgNameForPid(int pid);
    String getVersionForPid(int pid);
}
