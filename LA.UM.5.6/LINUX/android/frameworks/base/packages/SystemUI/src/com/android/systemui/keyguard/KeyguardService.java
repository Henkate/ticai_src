/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.systemui.keyguard;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.Debug;
import android.os.IBinder;
import android.os.Process;
import android.os.Trace;
import android.util.Log;
import android.content.Context;
import android.content.IntentFilter;
import android.os.SystemProperties;
import android.content.BroadcastReceiver;
import android.text.TextUtils;

import com.android.internal.policy.IKeyguardDrawnCallback;
import com.android.internal.policy.IKeyguardExitCallback;
import com.android.internal.policy.IKeyguardService;
import com.android.internal.policy.IKeyguardStateCallback;
import com.android.systemui.SystemUIApplication;
import android.app.admin.DevicePolicyManager;
import com.android.internal.widget.LockPatternUtils;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class KeyguardService extends Service {
    static final String TAG = "KeyguardService";
    static final String PERMISSION = android.Manifest.permission.CONTROL_KEYGUARD;

    private KeyguardViewMediator mKeyguardViewMediator;
	/* BEGIN: Added by chenyao, 2018/01/18   PN:Ticai system customization */
	private static final String KEY_LOCK = "intent.action_lock";
	private static final String KEY_UNLOCK = "intent.action_unlock";
	private static final String KEY_DISMISS_GUARD = "intent.action.dismiss_guard";
	private LockPatternUtils mLockPatternUtils;
	private static int unlockFailedAttempts = 0;
	/*END: Added by chenyao, 2018/01/18   PN:Ticai system customization */
	
    @Override
    public void onCreate() {
        ((SystemUIApplication) getApplication()).startServicesIfNeeded();
        mKeyguardViewMediator =
                ((SystemUIApplication) getApplication()).getComponent(KeyguardViewMediator.class);
        /* BEGIN: Added by chenyao, 2018/01/18   PN:Ticai system customization */
		mLockPatternUtils = new LockPatternUtils(this);
        IntentFilter filter = new IntentFilter();
        filter.addAction(KEY_LOCK);
        filter.addAction(KEY_UNLOCK);
        registerReceiver(mLockReceiver, filter);
        /*END: Added by chenyao, 2018/01/18   PN:Ticai system customization */
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    void checkPermission() {
        // Avoid deadlock by avoiding calling back into the system process.
        if (Binder.getCallingUid() == Process.SYSTEM_UID) return;

        // Otherwise,explicitly check for caller permission ...
        if (getBaseContext().checkCallingOrSelfPermission(PERMISSION) != PERMISSION_GRANTED) {
            Log.w(TAG, "Caller needs permission '" + PERMISSION + "' to call " + Debug.getCaller());
            throw new SecurityException("Access denied to process: " + Binder.getCallingPid()
                    + ", must have permission " + PERMISSION);
        }
    }

    private final IKeyguardService.Stub mBinder = new IKeyguardService.Stub() {

        @Override // Binder interface
        public void addStateMonitorCallback(IKeyguardStateCallback callback) {
            checkPermission();
            mKeyguardViewMediator.addStateMonitorCallback(callback);
        }

        @Override // Binder interface
        public void verifyUnlock(IKeyguardExitCallback callback) {
            Trace.beginSection("KeyguardService.mBinder#verifyUnlock");
            checkPermission();
            mKeyguardViewMediator.verifyUnlock(callback);
            Trace.endSection();
        }

        @Override // Binder interface
        public void keyguardDone(boolean authenticated, boolean wakeup) {
            Trace.beginSection("KeyguardService.mBinder#keyguardDone");
            checkPermission();
            // TODO: Remove wakeup
            mKeyguardViewMediator.keyguardDone(authenticated);
            Trace.endSection();
        }

        @Override // Binder interface
        public void setOccluded(boolean isOccluded, boolean animate) {
            Trace.beginSection("KeyguardService.mBinder#setOccluded");
            checkPermission();
            mKeyguardViewMediator.setOccluded(isOccluded, animate);
            Trace.endSection();
        }

        @Override // Binder interface
        public void dismiss(boolean allowWhileOccluded) {
            checkPermission();
            mKeyguardViewMediator.dismiss(allowWhileOccluded);
        }

        @Override // Binder interface
        public void onDreamingStarted() {
            checkPermission();
            mKeyguardViewMediator.onDreamingStarted();
        }

        @Override // Binder interface
        public void onDreamingStopped() {
            checkPermission();
            mKeyguardViewMediator.onDreamingStopped();
        }

        @Override // Binder interface
        public void onStartedGoingToSleep(int reason) {
            checkPermission();
            mKeyguardViewMediator.onStartedGoingToSleep(reason);
        }

        @Override // Binder interface
        public void onFinishedGoingToSleep(int reason, boolean cameraGestureTriggered) {
            checkPermission();
            mKeyguardViewMediator.onFinishedGoingToSleep(reason, cameraGestureTriggered);
        }

        @Override // Binder interface
        public void onStartedWakingUp() {
            Trace.beginSection("KeyguardService.mBinder#onStartedWakingUp");
            checkPermission();
            mKeyguardViewMediator.onStartedWakingUp();
            Trace.endSection();
        }

        @Override // Binder interface
        public void onScreenTurningOn(IKeyguardDrawnCallback callback) {
            Trace.beginSection("KeyguardService.mBinder#onScreenTurningOn");
            checkPermission();
            mKeyguardViewMediator.onScreenTurningOn(callback);
            Trace.endSection();
        }

        @Override // Binder interface
        public void onScreenTurnedOn() {
            Trace.beginSection("KeyguardService.mBinder#onScreenTurningOn");
            checkPermission();
            mKeyguardViewMediator.onScreenTurnedOn();
            Trace.endSection();
        }

        @Override // Binder interface
        public void onScreenTurnedOff() {
            checkPermission();
            mKeyguardViewMediator.onScreenTurnedOff();
        }

        @Override // Binder interface
        public void setKeyguardEnabled(boolean enabled) {
            checkPermission();
            mKeyguardViewMediator.setKeyguardEnabled(enabled);
        }

        @Override // Binder interface
        public void onSystemReady() {
            Trace.beginSection("KeyguardService.mBinder#onSystemReady");
            checkPermission();
            mKeyguardViewMediator.onSystemReady();
            Trace.endSection();
        }

        @Override // Binder interface
        public void doKeyguardTimeout(Bundle options) {
            checkPermission();
            mKeyguardViewMediator.doKeyguardTimeout(options);
        }

        @Override // Binder interface
        public void setCurrentUser(int userId) {
            checkPermission();
            mKeyguardViewMediator.setCurrentUser(userId);
        }

        @Override
        public void onBootCompleted() {
            checkPermission();
            mKeyguardViewMediator.onBootCompleted();
        }

        @Override
        public void startKeyguardExitAnimation(long startTime, long fadeoutDuration) {
            Trace.beginSection("KeyguardService.mBinder#startKeyguardExitAnimation");
            checkPermission();
            mKeyguardViewMediator.startKeyguardExitAnimation(startTime, fadeoutDuration);
            Trace.endSection();
        }

        @Override
        public void onActivityDrawn() {
            checkPermission();
            mKeyguardViewMediator.onActivityDrawn();
        }
		@Override
		public int ztlock(String passwd){
			checkPermission();
			if(mKeyguardViewMediator.isShowing()){
				return -1;
			}else{
				if(!TextUtils.isEmpty(passwd)){
					mLockPatternUtils.saveLockPassword(passwd,"",DevicePolicyManager.PASSWORD_QUALITY_NUMERIC,getUserId());
                	mKeyguardViewMediator.doKeyguardTimeout(null);
					SystemProperties.set("persist.zt.lock.passwd", passwd);
					return 0;
				}
				return -100;
			}
		}
		@Override
		public int ztunlock(String passwd){
			checkPermission();
			if(!mKeyguardViewMediator.isShowing()){
				unlockFailedAttempts++;
				return -1;
			}else{
				String password = passwd;
				String currentPasswd = SystemProperties.get("persist.zt.lock.passwd","123456");
				if(!TextUtils.isEmpty(password)){
					if(currentPasswd.equals(password)){
						mKeyguardViewMediator.keyguardDone(true);
						unlockFailedAttempts = 0;
						return 0;
					}else{
						unlockFailedAttempts++;
						return -2;
					}
				}
				unlockFailedAttempts++;
				return -100;
			}
		}
		@Override
		public int unlockFailedAttempts(){
			checkPermission();
			if(!mKeyguardViewMediator.isShowing()){
				return 0;
			}
			return unlockFailedAttempts;
		}
    };
    /* BEGIN: Added by chenyao, 2018/01/18   PN:Ticai system customization */
    private BroadcastReceiver mLockReceiver = new BroadcastReceiver (){

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(KEY_LOCK.equals(action)){
                String lockWord = intent.getStringExtra("lockWord");
				if(!TextUtils.isEmpty(lockWord)){
					mLockPatternUtils.saveLockPassword(lockWord,"",DevicePolicyManager.PASSWORD_QUALITY_NUMERIC,
                        context.getUserId());
                	mKeyguardViewMediator.doKeyguardTimeout(null);
					SystemProperties.set("persist.zt.lock.passwd", lockWord);
				}
            }else if(KEY_UNLOCK.equals(action)){
                String password = intent.getStringExtra("passWord");
				String currentPasswd = SystemProperties.get("persist.zt.lock.passwd","123456");
				if(currentPasswd.equals(password)){
					mKeyguardViewMediator.keyguardDone(true);
				}
            }
        }
    };
	/* END: Added by chenyao, 2018/01/18   PN:Ticai system customization */
}

