#include "szzt_gpio.h"
#include <linux/delay.h>
#include <linux/workqueue.h>
#include <linux/init.h>

static struct misc_data *misc;

int enable_otg;

static int __init get_runstate(char *str)
{
	printk("LHT get_runstate[%s]\n",str);
	if(strcmp("shutdown",str)==0)
	{
		enable_otg=1;
		printk("LHT get_runstate11111111111\n");
	}
	else
	{
		enable_otg=0;
		printk("LHT get_runstate11111111111\n");
	}
    return 0;
}

__setup("running.state=", get_runstate);

/**
 * gtp_parse_dt - parse platform infomation form devices tree.
 */
static int gtp_parse_dt(struct device *dev, struct misc_data *pdata)
{
	struct device_node *np = dev->of_node;
	int ret = -1;
	
	pdata->pr_en_gpio = of_get_named_gpio(np, "pr_en-gpios", 0);
	if(!gpio_is_valid(pdata->pr_en_gpio))
	{
		printk("invalid pr_en_gpio : %d\n", pdata->pr_en_gpio);
		goto gpio_get_err;
	}
	ret = gpio_request(pdata->pr_en_gpio, "pr_en_gpio");
	if (ret < 0)
	{
		goto gpio_get_err;
	}
	gpio_direction_output(pdata->pr_en_gpio, 1);

	
	
	pdata->otg_gpio = of_get_named_gpio(np, "otg-gpios", 0);
	if(!gpio_is_valid(pdata->otg_gpio))
	{
		printk("invalid otg_gpio : %d\n", pdata->otg_gpio);
		goto gpio_get_err;
	}
	ret = gpio_request(pdata->otg_gpio, "host_otg_gpio_ctl");
	if (ret < 0)
	{
		goto gpio_get_err;
	}

	gpio_direction_output(pdata->otg_gpio, 0);
#if 1
	if(enable_otg==0){

	gpio_set_value(pdata->otg_gpio,1);	
	mdelay(50);
	gpio_set_value(pdata->otg_gpio,0);	
	mdelay(50);
	gpio_set_value(pdata->otg_gpio,1);
		}
#endif	

/*	if(enable_otg==1)
	{
		gpio_set_value(pdata->otg_gpio,0);	
		mdelay(50);
		gpio_set_value(pdata->otg_gpio,1);	
		mdelay(50);
		gpio_set_value(pdata->otg_gpio,0);	
	}
*/
	
	return 0;
	
gpio_get_err:
	printk("szzt misc gtp_parse_dt init error\n");
	return ret;
}

static long misc_ioctl(struct file *filp,unsigned int cmd, unsigned long args)
{
	int ret;
	ret=0;
	mutex_lock(&misc->misc_lock);
	printk("LHT misc_ioctl args=[%ld]cmd[%d]\n",args,cmd);
	switch(cmd)
	{
		case IO_HOST_OTG_CTRL:
			if(args)
				gpio_set_value(misc->otg_gpio, 1);
			else
				gpio_set_value(misc->otg_gpio, 0);
			break;
		case IO_PR_EN_CTRL:
			if(args)
				gpio_set_value(misc->pr_en_gpio, 1);
			else
				gpio_set_value(misc->pr_en_gpio, 0);
			break;
		default:
			printk("unknow ioctl cmd\n");
			ret =  -EINVAL;
		break;
	}
	mutex_unlock(&misc->misc_lock);
	return ret;
}

static struct file_operations my_misc_fops = {
	.owner = THIS_MODULE,
	.unlocked_ioctl	 = misc_ioctl,
};

static struct miscdevice misc_ctrl_device = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = "szzt_gpio_ctl",
    .fops = &my_misc_fops,
    .mode = 0777
};

static int misc_plat_probe(struct platform_device *pdev)
{
	int ret;
	
	misc = devm_kzalloc(&pdev->dev, sizeof(*misc), GFP_KERNEL);
	if (misc == NULL)
	{
    	printk("Alloc GFP_KERNEL memory failed.");
    	return -ENOMEM;
	}
	
	mutex_init(&misc->misc_lock);
	ret = misc_register(&misc_ctrl_device);

	ret = gtp_parse_dt(&pdev->dev, misc);
	if(ret < 0)
		printk("parse device tree faild\n");

	printk("lht misc_plat_probe-------------\n");
	return ret;
}

static int misc_plat_remove(struct platform_device *pdev)
{
	gpio_free(misc->otg_gpio);
	misc_deregister(&misc_ctrl_device);
	kfree(misc);

	return 0;	
}

static struct of_device_id misc_dt_ids[] = {
	{ .compatible = "qcom,szzt_gpios" },	
	{ /* sentinel */ }
};

static struct platform_driver misc_drv = {
	.probe = misc_plat_probe,
	.remove = misc_plat_remove,
	.driver = {
		.name = "misc-ctrl",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(misc_dt_ids),
	},	
};

static int misc_init(void)
{
	printk("register szzt misc driver\n");
	platform_driver_register(&misc_drv);
	return 0;
}

static void misc_exit(void)
{
	platform_driver_unregister(&misc_drv);
	printk("bye\n");
}

late_initcall(misc_init);
module_exit(misc_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("KINGSEE");

