package com.android.settings;


import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.Environment;
import java.io.FileOutputStream;


import android.os.Build;
import android.view.Window;
import android.view.WindowManager;
import android.view.MenuItem;


public class ChangeThemeActivity extends Activity {

    ActionBar actionBar;
    SharedPreferences spSkin;
    private  ListView listView;
    private List<Map<String, Object>> mData;
    private   MyAdapter adapter;

    int postion_change = 1000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_them);
        spSkin = getSharedPreferences("setSkin", Context.MODE_PRIVATE);
        listView = (ListView) findViewById(R.id.list_view);
        mData = getData();
        adapter = new MyAdapter(this);
        listView.setAdapter(adapter);
        actionBar = getActionBar();

        ColorDrawable colorDrawable = new ColorDrawable(spSkin.getInt("color",Color.BLACK));
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        actionBar.setBackgroundDrawable(colorDrawable);
        actionBar.setDisplayHomeAsUpEnabled(true);
        Window window=ChangeThemeActivity.this.getWindow();
         if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.LOLLIPOP){
                                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            状态栏
                                window.setStatusBarColor(spSkin.getInt("color",Color.BLACK));
//            底部导航栏
                                window.setNavigationBarColor(spSkin.getInt("color",Color.BLACK));
                            }
    }

    private List<Map<String,Object>> getData() {
        List<Map<String,Object>> list = new ArrayList();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("color", 0xFF263238);
        map.put("img",R.drawable.round_originl);
        map.put("title", "原主题");
        map.put("isSelect", false);
        list.add(map);


        map = new HashMap<String, Object>();
        map.put("color", 0xFF288BD1);
        map.put("title", "蓝色主题");
        map.put("img",R.drawable.round_blue);
        map.put("isSelect", false);
        list.add(map);

        map = new HashMap<String, Object>();
        map.put("title", "黑色主题");
        map.put("img",R.drawable.round_black);
        map.put("color",0xFF2E2E2E);
        map.put("isSelect", false);
        list.add(map);

        map = new HashMap<String, Object>();
        map.put("title", "橙色主题");
        map.put("img",R.drawable.round_orange);
        map.put("color",0xFFFD643F);
        map.put("isSelect", false);
        list.add(map);

        map = new HashMap<String, Object>();
        map.put("title", "绿色主题");
        map.put("img",R.drawable.round_green);
        map.put("color",0xFF4CAF50);
        map.put("isSelect", false);
        list.add(map);

        map = new HashMap<String, Object>();
        map.put("title", "棕色主题");
        map.put("img",R.drawable.round_borwn);
        map.put("color",0xFF795548);
        map.put("isSelect", false);
        list.add(map);

        map = new HashMap<String, Object>();
        map.put("title", "红色主题");
        map.put("color",0xFFD50001);
        map.put("img",R.drawable.round_red);
        map.put("isSelect", false);
        list.add(map);


       int index  = spSkin.getInt("stlyeIndex",0);

       if (list.size()> index){
           Map<String,Object>  maps = list.get(index);
           maps.put("isSelect", true);
       }
        return list;
    }



    public final class ViewHolder{
        public ImageView colorView;
        public RadioButton radio_button;
        public TextView textView;
        public RadioGroup radioGroup;
        public Button button_id;
    }

    public class MyAdapter extends BaseAdapter {

        private LayoutInflater mInflater;



        public MyAdapter(Context context){
            this.mInflater = LayoutInflater.from(context);
        }
        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return mData.size();
        }

        @Override
        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }


        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {

            ViewHolder holder = null;
            if (convertView == null) {

                holder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.chanage_them_item, null);
                holder.colorView = (ImageView) convertView.findViewById(R.id.colorView);
                holder.radio_button = (RadioButton) convertView.findViewById(R.id.radio_button);
                holder.textView = (TextView) convertView.findViewById(R.id.text_view);
                holder.radioGroup  = (RadioGroup )convertView.findViewById(R.id.radioGroupID);
                holder.button_id  =(Button )convertView.findViewById(R.id.button_id);
                convertView.setTag(holder);

            }else {

                holder = (ViewHolder)convertView.getTag();
            }

            holder.colorView.setImageBitmap(BitmapFactory.decodeResource(getResources(),(int)mData.get(position).get("img")));
            holder.textView.setText((String)mData.get(position).get("title"));
            holder.radioGroup.setTag(position);
            holder.button_id.setTag(position);


            boolean isSelect = (boolean)mData.get(position).get("isSelect");
            if (isSelect){
                holder.radioGroup.check(holder.radio_button.getId());
            }else {

                holder.radioGroup.clearCheck();
            }


            holder.button_id.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int)v.getTag();

                    if (position == postion_change) return;

                    postion_change = position;
                    for(int i = 0; i< mData.size();i++) {
                        Map map = mData.get(i);
                        map.put("isSelect", false);
                        if (i == position) {
                            map.put("isSelect", true);
                            spSkin.edit().putInt("color", (Integer) map.get("color")).commit();
                            spSkin.edit().putInt("stlyeIndex",i).commit();
                            ColorDrawable colorDrawable = new ColorDrawable((Integer) map.get("color"));
                            actionBar.setBackgroundDrawable(colorDrawable);


                             Window window=ChangeThemeActivity.this.getWindow();
                            if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.LOLLIPOP){
                                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            状态栏
                                window.setStatusBarColor((Integer) map.get("color"));
//            底部导航栏
                                window.setNavigationBarColor((Integer) map.get("color"));
                            }
                            

                            try {
                                savaFileToSD("them",""+i);
                            } catch (Exception e) {
                                e.printStackTrace();

                            }

                        }

                    }

                    MyAdapter.this.notifyDataSetChanged();
                }
            });

            return convertView;
        }

    }



    //    往SD卡写入文件的方法
    public  void savaFileToSD(String filename, String filecontent) throws Exception {
        //如果手机已插入sd卡,且app具有读写sd卡的权限
        if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            filename = Environment.getExternalStorageDirectory().getCanonicalPath() + "/" + filename;
            //这里就不要用openFileOutput了,那个是往手机内存中写数据的
            FileOutputStream output = new FileOutputStream(filename);
            output.write(filecontent.getBytes());
            //将String字符串以字节流的形式写入到输出流中
            output.close();
            //关闭输出流
        }
    }


 @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == android.R.id.home){

            ChangeThemeActivity.this.onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }



}
