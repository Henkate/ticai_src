#!/bin/bash

cat $1 | while read line
do
    srcpath=`echo $line | cut -d \  -f 1`
    #pkgname=`echo $line | cut -d \  -f 2`
    topath=`echo $line | cut -d \  -f 2`
    destDir=`dirname $topath`
    toDir=$OUT$destDir
    #java -jar javamkdir.jar $topath
    mkdir -p $toDir
    cp $srcpath ${OUT}${topath}
    chown 0777 ${OUT}${topath}
    java -jar serviceinitadd.jar $2  $topath $3 $line
done
