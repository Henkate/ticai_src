#!/bin/bash

#basepath=$(cd `dirname $0`; pwd)
prodir=$(pwd)
#adb secure userdebug 8 msm8953
cd LA.UM.5.6/LINUX/android/

git checkout ./

./prebuilts/misc/linux-x86/ccache/ccache -M 50G

source build/envsetup.sh
source build/core/build_id.mk
lunch msm8953_64-$3
if [ $6 = "true" ];then
    time make clean
else
    echo "is Fast Build"
fi

########################################################
##package apk 

if [  -f "$CSLC_APKS_CONFIG" ]; then
   bash ./apk_config.sh $CSLC_APKS_CONFIG || exit 1
else	
	echo "$CSLC_APKS_CONFIG  not exist !!!!"
fi


########################################################
##package SERVICE 

if [  -f "$CSLC_SERVICE_CONFIG" ]; then
   rm device/qcom/msm8953_64/init.szzt.rc 
   bash  ./service_config.sh  $CSLC_SERVICE_CONFIG  device/qcom/msm8953_64/init.szzt.rc device/qcom/sepolicy/common/file_contexts || exit 1
else	
	echo "$CSLC_SERVICE_CONFIG  not exist !!!!"
fi


########################################################
##package FILES 

if [  -f "$CSLC_FILES_CONFIG" ]; then
   bash ./file_config.sh $CSLC_FILES_CONFIG || exit 1
else	
	echo "$CSLC_FILES_CONFIG  not exist !!!!"
fi
#######################################################
###fs_config
#java -jar ztfsconfig.jar system/core/libcutils/zt_fs_config.h $CSLC_SERVICE_CONFIG $CSLC_FILES_CONFIG noneedargs
#######################################################

#rm build/target/product/security/platform.*
#ln -s $CERT_FILE_DIR/platform.x509.pem build/target/product/security/platform.x509.pem
#ln -s $CERT_FILE_DIR/platform.pk8 build/target/product/security/platform.pk8

if [  -f ~/easypos_source/easypos.tar ]; then
cp -rf ~/easypos_source/easypos.tar ./vendor/szzt/etc/
test=#fortscsclient.tar
sed  -i '/#for easypos.tar/a\ PRODUCT_COPY_FILES += vendor/szzt/etc/easypos.tar:system/etc/easypos.tar' ./vendor/szzt/config/product.mk
else	
	echo "easypos.tar not exist !!!!"
fi

if [  -f ~/tscsclient_source/tscsclient.tar ]; then
cp -rf ~/tscsclient_source/tscsclient.tar  ./vendor/szzt/etc/
sed  -i '/#for tscsclient.tar/a\PRODUCT_COPY_FILES += vendor/szzt/etc/tscsclient.tar:system/etc/tscsclient.tar' ./vendor/szzt/config/product.mk
else	
	echo "tscsclient.tar not exist !!!!"
fi

mkdir -p ./build/cslcKey/
echo $CERT_FILE_PASSWORD > ./build/cslcKey/ps
cat ${DEVICE_LIST} >  ./vendor/szzt/etc/devices.lst
cp -vf $CERT_FILE_DIR/platform.pk8  ./build/target/product/security/platform.pk8
cp -vf $CERT_FILE_DIR/platform.x509.pem  ./build/target/product/security/platform.x509.pem

echo 'PRODUCT_PROPERTY_OVERRIDES += \
    ro.build.id='$TARGET_VERSION'' >> ./device/qcom/msm8953_64/msm8953_64.mk

echo 'PRODUCT_PROPERTY_OVERRIDES += \
    ro.build.display.id='$TARGET_VERSION'' >> ./device/qcom/msm8953_64/msm8953_64.mk

echo 'PRODUCT_PROPERTY_OVERRIDES += \
    qemu.hw.mainkeys=1' >> ./device/qcom/msm8953_64/msm8953_64.mk

#time make -j $4
#time make otapackage -j $4
time make dist -j $4 | tee ./compile_result.txt
echo "checking error......"
cat ./compile_result.txt | grep "#### make failed"

oemimagefile=$OUT/oem.img
ztsdcardimagefile=$OUT/ztsdcard.img
szztimagefile=$OUT/szzt.img

if [ $6 = "true" ];then
    time make oem_image
    time make ztsdcardimage
    time make szztimage
else
    if [ ! -f "$oemimagefile" ];then
        time make oem_image
    fi
    if [ ! -f "$ztsdcardimagefile" ];then
        time make ztsdcardimage
    fi
    if [ ! -f "$szztimagefile" ];then
        time make szztimage
    fi
fi

unzip -o out/dist/*img*.zip -d $OUT/

cd ${prodir}

source auto_msm8953_local_out.sh
source auto_msm8953_qfil_local_out.sh $2 $1 $3 $5 
toTargetFile=$OUTPUT_DIR/Zip/target_$VENDOR_PRODUCT_MODEL.$TARGET_VERSION.zip
if [  -f "$toTargetFile" ]; then
source auto_msm8953_ota.sh $toTargetFile $3
else
    echo ' '$toTargetFile'  not exist !!!! make ota package-----error'
fi

mkdir -p $OUTPUT_DIR
mkdir -p $OUTPUT_DIR/docs
mkdir -p $OUTPUT_DIR/ROM
mkdir -p $OUTPUT_DIR/OTA
mkdir -p $OUTPUT_DIR/FullPack
mkdir -p $OUTPUT_DIR/Zip

git log > $OUTPUT_DIR/docs/releaseNote.txt
