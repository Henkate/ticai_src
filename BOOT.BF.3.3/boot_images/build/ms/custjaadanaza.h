#ifndef CUSTJAADANAZA_H
#define CUSTJAADANAZA_H
/* ========================================================================
FILE: CUSTJAADANAZA

Copyright (c) 2017 by Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.        
=========================================================================== */

#ifndef TARGJAADANAZA_H
   #include "targjaadanaza.h"
#endif

#define FEATURE_QFUSE_PROGRAMMING
#define FEATURE_DLOAD_MEM_DEBUG
#define IMAGE_KEY_SBL1_IMG_DEST_ADDR SCL_SBL1_CODE_BASE
#define BOOT_TEMP_CHECK_THRESHOLD_DEGC
#define FEATURE_TPM_HASH_POPULATE




#endif /* CUSTJAADANAZA_H */
