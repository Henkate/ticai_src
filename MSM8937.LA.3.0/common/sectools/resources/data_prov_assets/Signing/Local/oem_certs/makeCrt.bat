set OPENSSL_CONF=%~dp0\openssl.cfg

openssl genrsa -out oem_rootca.key -3 2048
openssl req -new -key oem_rootca.key -x509 -out oem_rootca.crt -subj /C="US"/ST="CA"/L="SANDIEGO"/O="OEM"/OU="General OEM rootca"/CN="OEM ROOT CA" -days 7300 -set_serial 1 -config %~dp0\opensslroot.cfg -sha256
openssl x509 -in oem_rootca.crt -inform PEM -out qpsa_rootca.cer -outform DER 
openssl genrsa -out oem_attestca.key -3 2048
openssl req -new -key oem_attestca.key -out oem_attestca.csr -subj /C="US"/ST="CA"/L="SANDIEGO"/O="OEM"/OU="General OEM attestation CA"/CN="OEM attestation CA" -days 7300 -config %~dp0\opensslroot.cfg
openssl x509 -req -in oem_attestca.csr -CA oem_rootca.crt -CAkey oem_rootca.key -out oem_attestca.crt -set_serial 5 -days 7300 -extfile %~dp0\v3.ext -sha256
openssl x509 -in oem_attestca.crt -inform PEM -out qpsa_attestca.cer -outform DER
move oem_rootca.key qpsa_rootca.key 
move oem_attestca.key qpsa_attestca.key
openssl dgst -sha256 qpsa_rootca.cer > sha256rootcert.txt
