//====================================================================================================//
//================================ Meta-Scripts ReadMe.txt ===========================================//
//====================================================================================================//
//
//  Name:                                                                     
//    ReadMe_MSM8953.txt 
//
//  Description:                                                              
//    Basic script intro ReadMe.txt
//                                                                            
// Copyright (c) 2012 - 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
//
//
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who             what, where, why
// --------   ---             ---------------------------------------------------------
// 04/29/2016 c_sknven		  Updated T32 versions
// 02/01/2016 c_akaki		  Created for 8953.
// 10/30/2015 c_gunnan        Created for 8976.

This document describes the following:
    - Release notes, limitations, fixes, Lauterbach T32 approved versions.
    - Basic instructions for using features available for JTag on Qualcomm MSM chipsets.
    
Release Notes
    Date: 7/13/2015 - File created   
Existing limitations
    std_debug
        - Computer OS is assumed to be windows at this time. 
        - Reset scripts have limited functionality. This will affect scripts that reset the board to function (std_loadbuild and std_debug)
            - std_loadbuild - std_loadbuild loads all images necessary to get device to early appsboot (e.g. fastboot on LA). It then resets
                              the device and lets it boot up to appsboot. The first portion will work fine, the reset and subsequent bootup
                              may observe failures. Workaround: Manually reset the device after loadbuild has done its work, then you should 
                              see device get to appsboot.
            - std_debug     - std_debug loads desired ELF file and then resets the system to get to entry of desired subsystem (see below for 
                              for further explanation). The system may lock up during the reboot step and image entry will never be reached.
                              User will see "waiting to get to appsboot" for a long time. 
                              Workaround: If you see "waiting to get to appsboot" for a long time, do the following: 1.press the red 'STOP' button
                              in APPS0 Trace32 window. 2. Reset the device manually, 3. type 'system.mode.attach' in APPS0 Trace32 window. 
                              4. type 'continue' in APPS0 Trace 32 window. This is basically a manual reset and continuation of the scripts. 
                              When reattaching Apps0, the breakpoints will reassert, and 'continue' will continue the execution of the PRACTICE scripts
                              in Trace32. Please note that step 3 should be done soon after step 2 so that apps breakpoints will get set
                              before the system actually gets to appsboot (you have about 4-8 seconds with current builds).
		- std_debug's 'silent' mode (command line with no GUI's) must have either alternateelf specified or GLOBAL PRACTICE macro's must be properly 
			defined to give path to subsystem's elf (see std_loadsim more detailed note below as a related issue). 
		- std_savelogs_apps is not working in this release.
		- std_debug MBA not working .
		- Debugging through CTI,TPIU is not working in this release
		-RPM debugging is not avaible in this release
		- Additional note: For Android targets, make sure to set the 'persist' bit via adb commands: "adb wait-for-devices shell setprop persist.sys.ssr.enable_debug 1"
          This will keep apps' Peripheral Image Loader (PIL) from timing out on peripheral subsystem when it's booting up. This is only needed if user 
          desires to break processor during its initialization. If this is not set, and user has set a breakpoint that occurs prior to when peripheral 
          processor sends ACK back to apps' PIL, you'll see peripheral processor get reset (T32 will show 'RES/PWR DWN' soon after halt).
		
		


Release 7/25/2015
Lauterbach Trace32 builds verified:
TRACE32 PowerView for ARM64
   Interim Build (64-bit)
   Software Version: S.2014.12.000058805X
   Build: 58805.
---------------------------------------------
t32usbamd64.sys
   Jan 29 2013   USB
C:\T32\fcc.t32
   Dec  2 2014   Podbus (58805)
C:\T32\bin\windows64\t32marm64.exe
   Dec  2 2014   Host
   Dec  2 2014   Operation System
   Dec  2 2014   Debugger
C:\T32\fccarm.t32
   Dec  2 2014   Controller
   
Lauterbach Trace32 builds verified:
TRACE32 PowerView for ARM64
   Interim Build (64-bit)
   Software Version: S.2016.02.000069961
   Build: 69961.
---------------------------------------------
t32usbamd64.sys
   Jun 24 2010   USB Driver
C:\T32\fcc.t32
   Feb 10 2016   PowerDebug Communication Drv. (S.2016.02.000069961)
C:\T32\bin\windows64\t32marm64.exe
   Feb 10 2016   PowerView for ARM64           (S.2016.02.000069961)
C:\T32\fccarm.t32
   Feb  9 2016   PowerDebug Debug Driver       (S.2016.02.000069961)
    
This document assumes the following:
    An up to date Lauterbach Trace32 license and (for live debugging) working JTag pod.
    Python 2.6 or later is installed and  available to system path
    MSM target is properly powered. In the case of using std_Debug scripts
        target has software and successfully boots to subsystem of choice.
    Trace32 is installed and computer is successfully connected to Trace32 pod
    Using Windows for computer OS.
    See chipset help pdf for further details
        
A note on T32 path
    All the scripts described are embedded into t32 path, so they do not require absolute accessing.
    I.E. a simple "do std_debug" will invoke the std_debug script. 
    It is not necessary to call it directly (i.e. "do common\std_debug.cmm" is not necessary)
        
Help menus
    Many of the scripts have help menus to describe functionality and usage. 
    These can be accessed in the following way
        do std_debug help
        do std_cti help
        do std_loadsim help
        do std_loadsyms_adsp help 
        do std_loadsyms_mpss help 
        do std_loadsyms_rpm help 
        
        
std_loadbuild
    This is primarily a GUI which allows loading the a build via JTag to the target.
    It has some other features such as individually swapping out images (such as tz or sbl based images)
    You can also set T32's paths for the different image builds here. Pressing the 'map' button
    will intercom to other open T32 windows to change their paths as well.
    Deviceprogrammer is for internal use only.
        
std_debug
    std_debug is a suite of scripts intended to facilitate JTag attach to the core of choice.
    There are two primary features within std_debug:
        std_debug boot up debug
            This is designed to get the user attached to desired subsystem with specified breakpoints and options.
            If 'main' is specified as breakpoint, the subsystem will halt at entry. Otherwise the subsystem
            will go immediately from main(), setting all error breakpoints, which preserves system sync
            This is offered through the primary GUI (obtained by typing 'do std_debug' in the CLUSTER0_APPS0 T32 window) or
            by command line (type 'do std_debug help' in the CLUSTER0_APPS0 T32 window for command line instructions)
            
         
                



    
        
        
        
        
        
        
        
        
        
        
        
