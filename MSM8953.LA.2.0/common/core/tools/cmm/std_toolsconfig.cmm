//============================================================================
//  Name:                                                                     
//    std_toolsconfig.cmm 
//
//  Description:                                                              
//    Configures the path of the tools / scripts for a particular debug session
//                                                                            
// Copyright (c) 2012 - 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
//
//
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who     		what, where, why
// --------   ---     		---------------------------------------------------------
// 10/26/2015 c_gunnan      changed path for gen_buildflavor.cmm
// 07/13/2015 c_gunnan      wcnss path is added  for 8976
// 12/10/2012 AJCheriyan    Added support for multiple chipsets 	
// 07/10/2012 AJCheriyan    Created for B-family. 

// Supports 2 arguments
// ARG0 - Name of the Chipset for which path has to be setup
// ARG1 - Name of the Subsys for which path has to be setup
ENTRY &ARG0 &ARG1

// Create a global variable for every "subsystem" script folder
// Top level meta scripts
GLOBAL &METASCRIPTSDIR
// Common Scripts
GLOBAL &COMMON_METASCRIPTSDIR
// Generated Scripts
GLOBAL &GEN_METASCRIPTSDIR
// Sub-system level scripts
GLOBAL &MPSS_METASCRIPTSDIR
GLOBAL &RPM_METASCRIPTSDIR
GLOBAL &APPS_METASCRIPTSDIR
GLOBAL &ADSP_METASCRIPTSDIR
GLOBAL &WCNSS_METASCRIPTSDIR
GLOBAL &IPA_METASCRIPTSDIR
GLOBAL &VSS_METASCRIPTSDIR
GLOBAL &IPA_METASCRIPTSDIR
GLOBAL &BUILD_FLAVORDIR

&METASCRIPTSDIR=OS.PPD()
&GEN_METASCRIPTSDIR="&METASCRIPTSDIR/gen"
&MPSS_METASCRIPTSDIR="&METASCRIPTSDIR/mpss/&ARG0"
&COMMON_METASCRIPTSDIR="&METASCRIPTSDIR/common"
&COMMON_CHIPSET_METASCRIPTSDIR="&METASCRIPTSDIR/common/&ARG0"
&APPS_METASCRIPTSDIR="&METASCRIPTSDIR/apps/&ARG0"
&RPM_METASCRIPTSDIR="&METASCRIPTSDIR/rpm/&ARG0"
&ADSP_METASCRIPTSDIR="&METASCRIPTSDIR/adsp/&ARG0"
&SLPI_METASCRIPTSDIR="&METASCRIPTSDIR/slpi/&ARG0"
&WCNSS_METASCRIPTSDIR="&METASCRIPTSDIR/wcnss/&ARG0"
&VSS_METASCRIPTSDIR="&METASCRIPTSDIR/vss/&ARG0"
&IPA_METASCRIPTSDIR="&METASCRIPTSDIR/ipa/&ARG0"
&BUILD_FLAVORDIR="&METASCRIPTSDIR/../../../build/app"

// Set the path up to include the top level, SS specific scripts and apps specific script
PATH &METASCRIPTSDIR &COMMON_METASCRIPTSDIR &COMMON_CHIPSET_METASCRIPTSDIR &GEN_METASCRIPTSDIR &BUILD_FLAVORDIR &METASCRIPTSDIR/&ARG1/&ARG0 &APPS_METASCRIPTSDIR &APPS_METASCRIPTSDIR/&HLOS &METASCRIPTSDIR/apps &METASCRIPTSDIR/&ARG1 &METASCRIPTSDIR/adsp &METASCRIPTSDIR/slpi &METASCRIPTSDIR/mpss &METASCRIPTSDIR/apps &METASCRIPTSDIR/ipa &METASCRIPTSDIR/wcnss  &METASCRIPTSDIR/rpm


// Set APPS0 to have full system knowledge 
IF ((STR.UPR("&ARG1")=="APPS"))
(
	PATH + &MPSS_METASCRIPTSDIR &ADSP_METASCRIPTSDIR &SLPI_METASCRIPTSDIR &WCNSS_METASCRIPTSDIR &VSS_METASCRIPTSDIR &IPA_METASCRIPTSDIR &RPM_METASCRIPTSDIR
)

ENDDO
