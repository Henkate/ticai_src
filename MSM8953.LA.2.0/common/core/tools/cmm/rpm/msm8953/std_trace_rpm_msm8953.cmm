//============================================================================
//  Name:                                                                     
//    std_trace_rpm_msm8976.cmm 
//
//  Description:                                                              
//    Trace setup script for RPM
//                                                                            
// Copyright (c) 2012 - 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
//
//
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who             what, where, why
// --------   ---             ---------------------------------------------------------
// 08/03/2015 JBILLING        Minor cleanup and cycleaccurate,portsize feature
// 06/18/2015 JBILLING        Minor tpiu preference update
// 04/15/2015 JBILLING        Minior bug fix for 8996
// 03/04/2015 jbilling        Updated for 8996
// 06/18/2014 jbilling        Changed for 8994 cmm framework
// 01/07/2014 jbilling        Major Logic Update
// 09/28/2012 jbilling        Created for B-family.

//####################Declarations#######################

LOCAL &ArgumentLine
LOCAL &UTILITY &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10 &IARG11
LOCAL &TargetProcessor &Default_TPIU_Sink

//###################Arguments passed #####################
ENTRY %LINE &ArgumentLine
ENTRY &UTILITY &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10 &IARG11

//##################Defaults##########################
&TargetProcessor="rpm"
&Default_TPIU_Sink="tpiub" //for 8996 use TPIUB by default
&Default_TraceSource="rpm"




//#####################Select Subroutine###################
// Name of the utility we are calling
LOCAL &SUBROUTINE
// Any subroutine specific options
// Default exists for each subroutine
LOCAL &OPTION

// Input Argument 0 is the name of the utility
&SUBROUTINE="&UTILITY"
IF !(("&SUBROUTINE"!="GETTRACEDEFAULTS")||("&SUBROUTINE"!="STARTTRACING"))
(
    PRINT %ERROR "WARNING: UTILITY &UTILITY DOES NOT EXIST."
    GOTO EXIT
)


    // This should be created by some top level script. The setupenv for each proc would
    // set this up
     AREA.SELECT
    // Call the required utility
    GOSUB &SUBROUTINE &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10 &IARG11
    ENTRY &PASS &RVAL0 &RVAL1 &RVAL2

    GOTO EXIT
    

//////////////////////////////////////////////
//
//    GETTRACEDEFAULTS
//    Returns Trace default parameters for this target processor
//
//  assumed/example call:
//  do std_trace_rpm_&CHIPSET  GETTRACEDEFAULTS &tracesink &tracesource &CycleAccurate &traceconfiguration &Portsize 
//  where 
//      &tracesink="etb" 
//      &tracesource="rpm" 
//      &CycleAccurate="cycleaccurate" 
//      &traceconfiguration="fromcoldboot" 
//      &Portsize=="16bit" 
//////////////////////////////////////////////
GETTRACEDEFAULTS:
    LOCAL &ACTION &ARG0 &ARG1 &ARG2 &ARG3 &ARG4 &ARG5 &ARG6 &ARG7 &ARG8 &ARG9 &ARG10 &ARG11
    ENTRY &ACTION &ARG0 &ARG1 &ARG2 &ARG3 &ARG4 &ARG5 &ARG6 &ARG7 &ARG8 &ARG9 &ARG10 &ARG11

    LOCAL &given_tracesource &given_tracesink &given_cycleaccurate &given_traceconfiguration &given_portsize
    
    &given_tracesource=            string.lwr("&ARG0")
    &given_tracesink=           string.lwr("&ARG1")
    &given_cycleaccurate=       string.lwr("&ARG2")
    &given_traceconfiguration=  string.lwr("&ARG3")
    &given_portsize=            string.lwr("&ARG4")
    
    LOCAL &rvalue_tracesource &rvalue_tracesink &rvalue_tpiusink &rvalue_portsize &rvalue_cycleaccuratecommand &rvalue_traceconfiguration &rvalue_portsizecommand 
    
    
    //Set Defaults
    &rvalue_tracesource=            "&Default_TraceSource"    
    &rvalue_tracesink=              "none"
    &rvalue_tpiusink=                "&Default_TPIU_Sink" 
    &rvalue_portsize=               "16bit" 
    &rvalue_cycleaccuratecommand=    "off"
    &rvalue_traceconfiguration=        "fromcoldboot"
    &rvalue_portsizecommand=        "16."

                        
        //###########Trace sink configuration######
        IF ("&given_tracesink"=="etb")
        (
            &rvalue_tracesink="etb"
        )
        ELSE IF ("&given_tracesink"=="tpiua")
        (
            &rvalue_tracesink="tpiua"
        )
        ELSE IF ("&given_tracesink"=="tpiu_a")
        (
            &rvalue_tracesink="tpiua"
        )
        ELSE IF ("&given_tracesink"=="tpiub")
        (
            &rvalue_tracesink="tpiub"
        )
        ELSE IF ("&given_tracesink"=="tpiu_b")
        (
            &rvalue_tracesink="tpiub"
        )
        ELSE IF ("&given_tracesink"=="tpiu")
        (
            //do nothing. Default TPIU port should be set in GETTRACEDEFAULTS above
        )
        ELSE //TraceSink not specified. No tracing should occur
        (
            &rvalue_tracesink="none"
        )

        //################Other trace configurations###################
        IF (("&given_cycleaccurate"=="true")||("&given_cycleaccurate"=="on"))
        (
            &rvalue_cycleaccuratecommand="on"
        )
        
        IF ("&given_traceconfiguration"=="hotattach")
        (
            &rvalue_traceconfiguration=    "HOTATTACH"
        )
        IF ("&given_traceconfiguration"=="fromcoldboot")
        (
            &rvalue_traceconfiguration=    "fromcoldboot"
        )
        
        IF ("&given_portsize"=="4bit")
        (
            &rvalue_portsizecommand=4
        )
        IF ("&given_portsize"=="8bit")
        (
            &rvalue_portsizecommand=8
        )
        IF ("&given_portsize"=="16bit")
        (
            &rvalue_portsizecommand=16
        )

    ENDDO &rvalue_tracesource &rvalue_tracesink &rvalue_tpiusink &rvalue_portsize &rvalue_cycleaccuratecommand &rvalue_traceconfiguration &rvalue_portsizecommand 
        

    
    
    
//////////////////////////////////////////////
//
//    Main Tracing Functionality
//    assumes to be run on target processor's Jtag window
//    CPU doesn't need to be halted, but ETM must be accessible
//    (i.e. not in power collapse or clock gated)
//
//////////////////////////////////////////////
STARTTRACING:
    LOCAL &ACTION &ARG0 &ARG1 &ARG2 &ARG3 &ARG4
    ENTRY &ACTION &ARG0 &ARG1 &ARG2 &ARG3 &ARG4
    LOCAL &tracesource &tracesink &cycleaccuratecommand &traceconfiguration &portsizecommand

    &tracesource=              "&ARG0"
    &tracesink=                "&ARG1"
    &cycleaccuratecommand=     "&ARG2"
    &traceconfiguration=       "&ARG3"
    &portsizecommand=          "&ARG4"
            
    do hwio 
    
    IF (("&tracesink"=="tpiua")||("&tracesink"=="tpiub"))
    (
        SYStem.CONFIG.TPIU1.BASE DAP:0x06020000
        SYStem.CONFIG.ETF.BASE DAP:0x06025000 ;ETB/ETF
        SYStem.CONFIG.FUNNEL2.BASE DAP:0x06021000 ;Funnel 128bit IN_FUN0 8x1
        SYStem.CONFIG.FUNNEL3.BASE DAP:0x06023000 ;Merg Funnel 2x1
        SYStem.CONFIG.FUNNEL2.ATBSOURCE ETM 0 
        SYStem.CONFIG.FUNNEL3.ATBSOURCE FUNNEL2 0
        SYStem.CONFIG.ETF.ATBSOURCE FUNNEL3
        SYStem.CONFIG.TPIU1.ATBSOURCE ETF
        
        ETM.RESET
        ETM.CYCLEACCURATE &cycleaccuratecommand
        ETM.PORTSIZE &portsizecommand
        ETM.TRACEID 4.
        ETM.PORTMODE CONTINUOUS
        ETM.STALL OFF
        ETM.ON
        ETM.CLEAR

        TRACE.METHOD ANALYZER
        TRACE.CLOCK 1.MHz
        TRACE.INIT 
        TRACE.LIST
        TRACE
        
        wait.200ms
        
        ON ERROR GOSUB autofocusErrorHandler
            TRACE.AUTOFOCUS
        ON ERROR

        TRACE
        
        //if it's already running, start tracing.
        IF (STATE.RUN()==TRUE())
        (
            TRACE.ARM
        )
    )
    ELSE IF ("&tracesink"=="etb")
    (
        SYStem.CONFIG COREBASE E:0xE000E000
		SYStem.CONFIG ITMBASE E:0xE0000000
		SYStem.CONFIG DWTBASE E:0xE0001000
		SYStem.CONFIG ETMBASE E:0xE0041000
		SYStem.CONFIG.ETB.BASE DAP:0x06027000 ;ETB/ETF
		SYStem.CONFIG.FUNNEL3.BASE DAP:0x06021000 ;Funnel 128bit IN_FUN0 8x1
		SYStem.CONFIG.FUNNEL2.BASE DAP:0x06100000 ;QDA_CENTER
		SYStem.CONFIG.FUNNEL2.ATBSOURCE ETM 0 
		SYStem.CONFIG.FUNNEL3.ATBSOURCE FUNNEL2 3
		SYStem.CONFIG.ETB.ATBSOURCE FUNNEL3
        
        ETM.RESET
        ETM.CYCLEACCURATE &cycleaccuratecommand
        ETM.STALL OFF
        ETM.ON
        ETM.CLEAR

        TRACE.METHOD ONCHIP
        TRACE.CLOCK 2.5MHz
        TRACE.INIT 
        TRACE.LIST
        TRACE
        
        //if it's already running, start tracing.
        IF (STATE.RUN()==TRUE())
        (
            TRACE.ARM
        )
    )
    ELSE 
    (
        PRINT %ERROR "Error! std_trace_rpm_&CHIPSET - Unknown TraceSink Value: &tracesink"
        GOTO FATALEXIT
    )


RETURN
  

autofocusErrorHandler:
    ON ERROR
    area
    print %error " Autofocus Error indicate that Trace signal lines likely have some physical path problem."
    print %error " Make sure that TPIU cable is plugged in, and that the appropriate ETM switch below the CDP"
    print %error "     is set to on."
    GOTO EXIT
    
       
print_wrongt32version:
    area
    print %error "Syntax error detected. Please update your Trace32 version to latest, "
    print %error " "
    print %error "exiting..."
    goto FATALEXIT
    
    

EXIT:
    ENDDO

FATALEXIT:
    END

 
