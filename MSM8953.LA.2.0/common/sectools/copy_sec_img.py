import shutil,os.path


home_path=".."
secimage_path="./common/sectools/secimage_output/8953"
tz_path=home_path+"/TZ.BF.4.0.5/trustzone_images/build/ms/bin/SANAANAA"
boot_path=home_path+"/BOOT.BF.3.3/boot_images/build/ms/bin"
adsp_path=home_path+"/ADSP.8953.2.8.2/adsp_proc/obj/8953"
modem_path=home_path+"/MPSS.TA.2.3/modem_proc/build/ms/bin/8953.genw3k.prod"
rpm_path=home_path+"/RPM.BF.2.4/rpm_proc/build/ms/bin"
wcnss_path=home_path+"/CNSS.PR.4.0/wcnss_proc/build/ms/bin"
video_path=home_path+"/VIDEO.VE.4.2/venus_proc/build/bsp/asic/build/PROD/mbn"
android_path=home_path+"/LA.UM.5.6/LINUX/android/out/target/product/msm8953_64"


shutil.copy((secimage_path+"/sbl1/sbl1.mbn"), (boot_path+"/JAASANAZ/sbl1.mbn"))
shutil.copy((secimage_path+"/prog_emmc_firehose_ddr/prog_emmc_firehose_8953_ddr.mbn"), (boot_path+"/JAADANAZ/prog_emmc_firehose_8953_ddr.mbn"))
shutil.copy((secimage_path+"/prog_emmc_firehose_lite/prog_emmc_firehose_8953_lite.mbn"), (boot_path+"/JAADANAZ/prog_emmc_firehose_8953_lite.mbn"))
shutil.copy((secimage_path+"/validated_emmc_firehose_ddr/validated_emmc_firehose_8953_ddr.mbn"), (boot_path+"/JAADANAZ/validated_emmc_firehose_8953_ddr.mbn"))
shutil.copy((secimage_path+"/validated_emmc_firehose_lite/validated_emmc_firehose_8953_lite.mbn"), (boot_path+"/JAADANAZ/validated_emmc_firehose_8953_lite.mbn"))
shutil.copy((secimage_path+"/qsee/tz.mbn"), (tz_path+"/tz.mbn"))
shutil.copy((secimage_path+"/devcfg/devcfg.mbn"), (tz_path+"/devcfg.mbn"))
shutil.copy((secimage_path+"/appsbl/emmc_appsboot.mbn"), (android_path+"/emmc_appsboot.mbn"))
shutil.copy((secimage_path+"/adsp/adsp.mbn"), (adsp_path+"/signed/adsp.mbn"))
shutil.copy((secimage_path+"/mba/mba.mbn"), (modem_path+"/mba.mbn"))
shutil.copy((secimage_path+"/modem/modem.mbn"), (modem_path+"/qdsp6sw.mbn"))
shutil.copy((secimage_path+"/rpm/rpm.mbn"), (rpm_path+"/8953/rpm.mbn"))
shutil.copy((secimage_path+"/wcnss/wcnss.mbn"), (wcnss_path+"/8953/reloc/wcnss.mbn"))
shutil.copy((secimage_path+"/venus/venus.mbn"), (video_path+"/reloc/signed/venus.mbn"))
shutil.copy((secimage_path+"/sampleapp32/smplap32.mbn"), (tz_path+"/smplap32.mbn"))
shutil.copy((secimage_path+"/sampleapp64/smplap64.mbn"), (tz_path+"/smplap64.mbn"))
shutil.copy((secimage_path+"/isdbtmm/isdbtmm.mbn"), (tz_path+"/isdbtmm.mbn"))
shutil.copy((secimage_path+"/widevine/widevine.mbn"), (tz_path+"/widevine.mbn"))
shutil.copy((secimage_path+"/cmnlib32/cmnlib.mbn"), (tz_path+"/cmnlib.mbn"))
shutil.copy((secimage_path+"/cmnlib64/cmnlib64.mbn"), (tz_path+"/cmnlib64.mbn"))
shutil.copy((secimage_path+"/keymaster/keymaster.mbn"), (tz_path+"/keymaster.mbn"))
shutil.copy((secimage_path+"/mdtp/mdtp.mbn"), (tz_path+"/mdtp.mbn"))
shutil.copy((secimage_path+"/cppf/cppf.mbn"), (tz_path+"/cppf.mbn"))
shutil.copy((secimage_path+"/qmpsecap/qmpsecap.mbn"), (tz_path+"/qmpsecap.mbn"))



















