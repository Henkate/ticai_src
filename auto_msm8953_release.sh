#########################################################################
# File Name: auto_out.sh
# Author: 
# mail: 
# Created Time: 2016年11月22日 星期二 16时41分09秒
#########################################################################
#!/bin/bash

PROJECT_DT1=$(date +%Y%m%d)
PROJECT_DT2=$(date +%Y%m%d%H%m)
PROJECT_DT=$(date +%Y%m%d%H%m)

prodir=$(pwd)

if [ $TARGET_BUILD_VARIANT = "user" ];then
	if [ $PLATFORM_ENABLE_ADB = "true" ];then
	    sw_dir=/media/data/sw_release/user/$1/${BUILD_ID}
	else
	    sw_dir=/media/data/sw_release/user/$1/${BUILD_ID}
	fi
else
	sw_dir=/media/data/sw_release/userdebug/$1/${BUILD_ID}
fi

if [ ! -d "$sw_dir" ]; then
     mkdir -p $sw_dir
fi

rm ${TARGET_PRODUCT}*.zip QFIL_*.zip

local_sw_dir=${prodir}/SW_DIR

cp ${local_sw_dir}/*  ${sw_dir}/
chmod  0777 ${sw_dir}/*
